Source: filemanager-actions
Section: gnome
Priority: optional
Maintainer: Carlos Maddela <e7appew@gmail.com>
Build-Depends: debhelper (>= 11),
               gnome-doc-utils (>= 0.3.2),
               gtk-doc-tools,
               intltool (>= 0.50.2),
               libcaja-extension-dev (>= 1.16.0),
               libglib2.0-dev (>= 2.32.1),
               libgtk-3-dev (>= 3.4.1),
               libgtop2-dev (>= 2.28.4),
               libnautilus-extension-dev (>= 3.4.1),
               libnemo-extension-dev (>= 1.8),
               libxml2-dev (>= 2.7.8),
               uuid-dev (>= 1.6.2)
Build-Depends-Indep: inkscape, jdupes, pandoc
Standards-Version: 4.3.0
Homepage: https://gitlab.gnome.org/GNOME/filemanager-actions
Vcs-Browser: https://salsa.debian.org/debian/filemanager-actions
Vcs-Git: https://salsa.debian.org/debian/filemanager-actions.git
Rules-Requires-Root: no

Package: filemanager-actions
Architecture: any
Depends: filemanager-actions-data (<< ${source:Version}.1~),
         filemanager-actions-data (>= ${source:Version}),
         filemanager-actions-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: nautilus-actions (<< 3.4-1~)
Breaks: nautilus-actions (<< 3.4-1~)
Recommends: fma-filemanager-extension | fma-nautilus-extension
Suggests: filemanager-actions-doc
Description: File manager extension to allow user-defined actions
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides the graphical interface for creating and editing
 file-manager actions.

Package: filemanager-actions-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Common data files for FileManager-Actions
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides the common data files used by FileManager-Actions.

Package: filemanager-actions-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: HTML user documentation for FileManager-Actions
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides HTML user documentation for FileManager-Actions.

Package: filemanager-actions-libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Core FileManager-Actions libraries
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides the core libraries used by FileManager-Actions.

Package: filemanager-actions-dev
Section: devel
Architecture: any
Multi-Arch: same
Depends: filemanager-actions-libs (= ${binary:Version}), ${misc:Depends}
Description: Development files for FileManager-Actions
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides headers, shared libraries and development
 documentation for FileManager-Actions.

Package: fma-nautilus-extension
Architecture: any
Multi-Arch: same
Depends: filemanager-actions-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: fma-filemanager-extension
Enhances: nautilus
Recommends: filemanager-actions
Description: Nautilus extension to allow user-defined file-manager actions
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides the Nautilus extension to allow the execution of
 the user-defined file-manager actions.

Package: fma-caja-extension
Architecture: any
Multi-Arch: same
Depends: filemanager-actions-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: fma-filemanager-extension
Enhances: caja
Recommends: filemanager-actions
Description: Caja extension to allow user-defined file-manager actions
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides the Caja extension to allow the execution of the
 user-defined file-manager actions.

Package: fma-nemo-extension
Architecture: any
Multi-Arch: same
Depends: filemanager-actions-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: fma-filemanager-extension
Enhances: nemo
Recommends: filemanager-actions
Description: Nemo extension to allow user-defined file-manager actions
 FileManager-Actions is an extension for GNOME-based file managers, which
 allows the user to add arbitrary programs to be launched through the file
 manager's context menu, based on the current selection.
 .
 The project was formerly known as Nautilus-Actions, but has changed its
 name since it now supports Caja and Nemo, in addition to Nautilus.
 .
 This package provides the Nemo extension to allow the execution of the
 user-defined file-manager actions.

Package: nautilus-actions
Section: oldlibs
Architecture: any
Depends: filemanager-actions, fma-nautilus-extension, ${misc:Depends}
Description: transitional package for FileManager-Actions
 Nautilus actions is an extension for Nautilus, the GNOME file manager.
 It allows the configuration of programs to be launched on files selected
 in the Nautilus interface.
 .
 The project has undergone a name change since other GNOME-based file
 managers, like Caja and Nemo, are now supported as well.
 .
 This package provides an easy way to upgrade to FileManager-Actions.
 It can be safely removed.
