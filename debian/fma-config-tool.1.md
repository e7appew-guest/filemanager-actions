% fma-config-tool(1) fma-config-tool Debian Manual
%
% 2018-12-01

# NAME

fma-config-tool - FileManager-Actions Configuration Tool

# SYNOPSIS

fma-config-tool [*OPTION...*]

# OPTIONS

## Help Options:

-h, \--help
: Show help options

\--help-all
: Show all help options

\--help-gtk
: Show GTK+ Options

## Application Options:

-n, \--non-unique
: Set it to run multiple instances of the program [unique]

-v, \--version
: Output the version number, and exit gracefully [no]

\--display=*DISPLAY*
: X display to use

# DESCRIPTION

`FileManager-Actions` is an extension for GNOME-based file managers which
allows arbitrary programs to be launched through the file manager's context
menu. Currently, Nautilus, Caja and Nemo are supported.

Each time you right-click on one or more files in the file manager, or any
part of the background of the currently opened folder, `FileManager-Actions`
will look at its configured actions to see if a program has been setup for
this selection. If so, it will add an item to the menu that allows you to
execute the program on the selected files or folders.

Configurations are stored in GSettings for speed and integration with
other GNOME programs. Configs can be easily shared using the "Import/Export
assistant" menu items in the Tools menu of `fma-config-tool`.

# BUGS

Please report bugs in `FileManager-Actions` to
<submit@bugs.debian.org>. The current bug list may be viewed at
<https://bugs.debian.org/filemanager-actions>.

# AUTHOR

`FileManager-Actions` was written by Rodrigo Moya <rodrigo@novell.com>,
Frederic Ruaudel <grumz@grumz.net>, Pierre Wieser <pwieser@trychlos.org>,
and contributors.

This manual page was written by Christine Spang <christine@debian.org>
and Alice Ferrazzi <aliceinwire@gnumerica.org>, for the Debian project
(but may be used by others).

# LICENSING

Both the `FileManager-Actions` source code and this man page are licensed
under the GNU General Public License.
